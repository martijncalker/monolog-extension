<?php

namespace MartijnCalker\Monolog;

use Exception;

class Logger extends \Monolog\Logger
{
    const TRACE = 50;

    protected static $levels = [
        self::TRACE     => 'TRACE',
        self::DEBUG     => 'DEBUG',
        self::INFO      => 'INFO',
        self::NOTICE    => 'NOTICE',
        self::WARNING   => 'WARNING',
        self::ERROR     => 'ERROR',
        self::CRITICAL  => 'CRITICAL',
        self::ALERT     => 'ALERT',
        self::EMERGENCY => 'EMERGENCY',
    ];

    /**
     * Adds a log record at the TRACE level.
     *
     * @param  string  $message The log message
     * @param  array   $context The log context
     * @return Boolean Whether the record has been processed
     */
    public function addTrace($message, array $context = array())
    {
        return $this->addRecord(static::TRACE, $message, $context);
    }

    /**
     * Adds a log record at the TRACE level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param  string  $message The log message
     * @param  array   $context The log context
     * @return Boolean Whether the record has been processed
     */
    public function trace($message, array $context = array())
    {
        return $this->addRecord(static::TRACE, $message, $context);
    }

    /**
     * @param Exception $exception
     * @param int $key
     * @return array
     */
    public static function handleExceptionRecursive(Exception $exception, int $key = 0): array
    {
        $array = [
            $key => [
                'class' => (string) get_class($exception),
                'code' => $exception->getCode(),
                'file' => (string) $exception->getFile(),
                'index' => (int) $key,
                'line' => (int) $exception->getLine(),
                'message' => (string) $exception->getMessage(),
                'trace' => (string) $exception->getTraceAsString(),
            ],
        ];

        if (null !== $exception->getPrevious()) {
            $array = array_merge(
                $array,
                self::handleExceptionRecursive($exception->getPrevious(), ++$key)
            );
        }

        return $array;
    }

    /**
     * @param Exception $exception
     * @return bool
     */
    public function exception(Exception $exception)
    {
        return $this->addRecord(
            static::ERROR,
            $exception->getMessage(),
            [
                'exception' => self::handleExceptionRecursive($exception),
            ]
        );
    }
}
